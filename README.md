**Seek**

* This project was created in IntelliJ using Java 8.

* The data is created in a HashMap in local memory, for testing the system performs an initial charge for Class src / Catalog. It is also possible to change the product data easily.

* The business logic was created in class src / Checkout

* The tests were performed in JUnit, to view them just run the src / test class.