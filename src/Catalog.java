import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;


public class Catalog {

   public static HashMap<String, Product> products = new HashMap<String, Product>();

    public static void set() {

        Product prd1 = new Product();
        prd1.setNome(" Super iPad ");
        prd1.setSpecialPrice(499.99);
        prd1.setPrice(549.99 );
        products.put("ipd", prd1);


        Product prd2 = new Product();
        prd2.setNome("MacBook Pro");
        prd2.setPrice(1399.99 );
        products.put("mbp", prd2);


        Product prd3 = new Product();
        prd3.setNome("Apple TV");
        prd3.setPrice(109.50 );
        products.put("atv", prd3);


        Product prd4 = new Product();
        prd4.setNome("VGA adapter");
        prd4.setPrice(30.00);
        products.put("vga", prd4);

    }

}
