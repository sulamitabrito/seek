import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Sulamita on 07/06/2016.
 */
public class Checkout {
    Integer countAtv = 0;
    Double total = 0.0;
    Integer countIpd = 0;
    Boolean bonus = false;
    boolean vga = false;



    public void scan(String item) {

        if(item == "atv"){
            countAtv ++;
        }
        if (item =="ipd") {
            countIpd ++;
        }
        if (item =="mbp"){
            bonus = true;
            total += calc(item);
        }
        if (item == "vga"){
            vga = true;
            total +=calc(item);
        }

    }

    private Double calcAtv() {
        Double atvPrice = 0.0;
        if (countAtv >=3) {
            atvPrice= 2* calc("atv");
        }
        else {
            atvPrice = calc("atv") * countAtv;
        }
        return atvPrice;
        }

    private Double calcIpd() {
        Double ipdPrice;
        if (countIpd>4){
            ipdPrice = Catalog.products.get("ipd").getSpecialPrice() * countIpd;
        }
        else {
            ipdPrice = calc("ipd") * countIpd;
        }
        return ipdPrice;
    }

    public Double calc (String item){
       Double value= Catalog.products.get(item).getPrice();
        return value;
    }

    public void total() {
        total += calcAtv();
        total += calcIpd();
        if (vga && bonus == true){
            total -= calc("vga");
        }

        System.out.println(total);
    }


}
