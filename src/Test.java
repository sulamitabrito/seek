import junit.framework.TestCase;

/**
 * Created by Sulamita on 07/06/2016.
 */
public class Test extends TestCase {
    String atv = "atv";
    String vga = "vga";
    String ipd = "ipd";
    String mbp = "mbp";
    
    public Test() {
        Catalog.set();
    }

    public void test1() {

        Checkout co = new Checkout();
        co.scan(atv);
        co.scan(atv);
        co.scan(atv);
        co.scan(vga);
        co.total();


    }

    public void test2(){

        Checkout co = new Checkout();
        co.scan(atv);
        co.scan(ipd);
        co.scan(ipd);
        co.scan(atv);
        co.scan(ipd);
        co.scan(ipd);
        co.scan(ipd);

        co.total();
    }

    public void test3(){
        Checkout co = new Checkout();
        co.scan(mbp);
        co.scan(vga);
        co.scan(ipd);
        co.total();
    }
}
